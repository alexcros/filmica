package com.keepcoding.filmica.view.search

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.keepcoding.filmica.R
import com.keepcoding.filmica.data.Film
import com.keepcoding.filmica.data.FilmsRepo
import kotlinx.android.synthetic.main.fragment_search.*
import kotlinx.android.synthetic.main.layout_error.*
import kotlinx.android.synthetic.main.search_error.*

class SearchFragment : Fragment() {
    interface OnItemClickListener {
        fun onItemClicked(film: Film)
    }

    private var searchStop = false
    lateinit var listener: OnItemClickListener

    val adapter: SearchAdapter by lazy {
        val instance = SearchAdapter()

        instance
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is OnItemClickListener) {
            listener = context
        }
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        listFilms.adapter = adapter

        searchText.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                s?.let {
                    val query = s.toString()
                    if (query.length > 2) {
                        searchStop = false
                        loader.visibility = View.VISIBLE
                        searchMovies(query)
                    } else {
                        searchStop = true
                        loader.visibility = View.GONE
                    }

                    listFilms.visibility = View.INVISIBLE
                }
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            }

        })
    }

    private fun searchMovies(query: String) {
        FilmsRepo.searchMovies(query, context!!,
            { films ->
                if (!films.isEmpty()) {
                    searchError.visibility = View.GONE
                    layoutError?.visibility = View.GONE
                    listFilms.visibility = View.VISIBLE
                    adapter.setFilms(films)
                } else {
                    searchError.visibility = View.VISIBLE
                }

                loader.visibility = View.GONE
            },
            { error ->
                loader?.visibility = View.GONE
                //listFilms.visibility = View.INVISIBLE
                listFilms.visibility = View.INVISIBLE
                layoutError?.visibility = View.VISIBLE

                error.printStackTrace()
            })
    }
}