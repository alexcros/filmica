package com.keepcoding.filmica.view.trending

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.keepcoding.filmica.R
import com.keepcoding.filmica.data.FilmsRepo
import com.keepcoding.filmica.view.films.FilmsFragment
import com.keepcoding.filmica.view.util.Constants.Companion.language
import com.keepcoding.filmica.view.util.Constants.Companion.sort
import com.keepcoding.filmica.view.util.ItemOffsetDecoration
import kotlinx.android.synthetic.main.fragment_films.*
import kotlinx.android.synthetic.main.layout_error.*

/**
 * Created by alexandre on 27/11/18.
 */
class TrendingFragment : Fragment() {

    lateinit var listener: FilmsFragment.OnItemClickListener
    private var trendingPage = 1

    private val trendingFilms: RecyclerView by lazy {
        val instance = view!!.findViewById<RecyclerView>(R.id.trending_films)
        instance.addItemDecoration(ItemOffsetDecoration(R.dimen.offset_grid))
        instance.setHasFixedSize(true)
        instance
    }

    private val adapter: TrendingAdapter by lazy {
        val instance = TrendingAdapter()
        instance
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_trending, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        trendingFilms.adapter = adapter
        btnRetry?.setOnClickListener { reload() }
        setRecyclerViewScrollListener()
    }

    override fun onResume() {
        super.onResume()
        this.reload()
    }

    private fun reload() {
        FilmsRepo.trendingFilms(context!!,
            trendingPage.toString(),
            language,
            sort,
            { films ->
                progress?.visibility = View.INVISIBLE
                layoutError?.visibility = View.INVISIBLE
                trendingFilms.visibility = View.VISIBLE
                adapter.setFilms(films)
            },
            { error ->
                progress?.visibility = View.INVISIBLE
                trendingFilms.visibility = View.INVISIBLE
                layoutError?.visibility = View.VISIBLE

                error.printStackTrace()
            })
    }

    private fun setRecyclerViewScrollListener() {
        trendingFilms.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(list: RecyclerView, newState: Int) {
                super.onScrollStateChanged(list, newState)

                val linearLayout: LinearLayoutManager = list.layoutManager as LinearLayoutManager
                var lastVisibleItemPosition = linearLayout.findLastVisibleItemPosition()
                val totalItemCount = list.layoutManager!!.itemCount

                if (totalItemCount == lastVisibleItemPosition + 1) {
                    loadNewPageDiscover(list)
                    adapter.notifyItemRangeInserted(lastVisibleItemPosition + 1, adapter.itemCount)
                }

            }

            private fun loadNewPageDiscover(list: RecyclerView) {
                trendingPage++

                FilmsRepo.trendingAddMoreFilms(
                    context!!,
                    trendingPage.toString(),
                    { films ->
                        progress?.visibility = View.INVISIBLE
                        layoutError?.visibility = View.INVISIBLE
                        list.visibility = View.VISIBLE
                        adapter.setFilms(films)
                    },
                    { error ->
                        progress?.visibility = View.INVISIBLE
                        list.visibility = View.INVISIBLE
                        layoutError?.visibility = View.VISIBLE

                        error.printStackTrace()
                    },
                    language,
                    sort
                )
            }

        })
    }


}
