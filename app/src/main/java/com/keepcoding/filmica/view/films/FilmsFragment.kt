package com.keepcoding.filmica.view.films

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.keepcoding.filmica.R
import com.keepcoding.filmica.data.Film
import com.keepcoding.filmica.data.FilmsRepo
import com.keepcoding.filmica.view.util.Constants.Companion.language
import com.keepcoding.filmica.view.util.Constants.Companion.sort
import com.keepcoding.filmica.view.util.ItemOffsetDecoration
import kotlinx.android.synthetic.main.fragment_films.*
import kotlinx.android.synthetic.main.layout_error.*

class FilmsFragment : Fragment() {

    lateinit var listener: OnItemClickListener
    private var page: Int = 1

    private val list: RecyclerView by lazy {
        val instance = view!!.findViewById<RecyclerView>(R.id.list_films)
        instance.addItemDecoration(ItemOffsetDecoration(R.dimen.offset_grid))
        instance.setHasFixedSize(true)

        instance
    }

    private val adapter: FilmsAdapter by lazy {
        val instance = FilmsAdapter { film ->
            this.listener.onItemClicked(film)
        }

        instance
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is OnItemClickListener) {
            listener = context
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_films, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        list.adapter = adapter

        btnRetry?.setOnClickListener { reload() }

        reload()

        setRecyclerViewScrollListener()
    }

    fun reload() {
        FilmsRepo.discoverFilms(
            context!!, page.toString(),
            { films ->
                progress?.visibility = View.INVISIBLE
                layoutError?.visibility = View.INVISIBLE
                list.visibility = View.VISIBLE
                adapter.setFilms(films)
            },
            { error ->
                progress?.visibility = View.INVISIBLE
                list.visibility = View.INVISIBLE
                layoutError?.visibility = View.VISIBLE

                error.printStackTrace()
            },
            language,
            sort
        )
    }

    private fun setRecyclerViewScrollListener() {
        list.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(list: RecyclerView, newState: Int) {
                super.onScrollStateChanged(list, newState)

                val linearLayout: LinearLayoutManager = list.layoutManager as LinearLayoutManager
                var lastVisibleItemPosition = linearLayout.findLastVisibleItemPosition()
                //var lastVisibleItemPosition = list.childCount
                val totalFilmsCount = list.layoutManager!!.itemCount

                if (totalFilmsCount == lastVisibleItemPosition + 1) {
                    page++

                    FilmsRepo.discoverAddMoreFilms(
                        context!!, page.toString(),
                        { films ->
                            progress?.visibility = View.INVISIBLE
                            layoutError?.visibility = View.INVISIBLE
                            list.visibility = View.VISIBLE
                            adapter.setFilms(films) //, lastVisibleItemPosition + 1)
                        },
                        { error ->
                            progress?.visibility = View.INVISIBLE
                            list.visibility = View.INVISIBLE
                            layoutError?.visibility = View.VISIBLE
                            error.printStackTrace()
                        },
                        language,
                        sort
                    )
                }
            }
        })
    }

    interface OnItemClickListener {
        fun onItemClicked(film: Film)
    }

}
