package com.keepcoding.filmica.view.search


import android.graphics.Bitmap
import android.graphics.Color
import android.support.v4.content.ContextCompat
import android.support.v7.graphics.Palette
import android.view.View
import com.keepcoding.filmica.R
import com.keepcoding.filmica.data.Film
import com.keepcoding.filmica.view.util.BaseFilmAdapter
import com.keepcoding.filmica.view.util.BaseFilmHolder
import com.keepcoding.filmica.view.util.SimpleTarget
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_search.view.*

/**
 * Created by alexandre on 28/11/18.
 */

class SearchAdapter(
): BaseFilmAdapter<SearchAdapter.SearchViewHolder>(
    layoutItem = R.layout.item_search,
    holderCreator = { view -> SearchAdapter.SearchViewHolder(view) }
) {
    class SearchViewHolder(view: View): BaseFilmHolder(view) {
        override fun bindFilm(film: Film) {
            super.bindFilm(film)

            with(itemView) {
                labelTitle.text = film.title
                loadImage(film)
            }
        }

        private fun loadImage(film: Film) {
            val target = SimpleTarget(
                successCallback = { bitmap, from ->
                    itemView.imgPoster.setImageBitmap(bitmap)
                    setColourFrom(bitmap)
                }
            )
            itemView.imgPoster.tag = target

            Picasso.get().load(film.getPosterUrl()).error(R.drawable.placeholder).into(target)

        }

        private fun setColourFrom(bitmap: Bitmap) {
            Palette.from(bitmap).generate { palette ->
                var lightSwatch = palette?.lightVibrantSwatch ?: palette?.lightMutedSwatch
                val lightColour = lightSwatch?.rgb
                    ?: ContextCompat.getColor(itemView.context!!, R.color.colorPrimary)

                var overlayColour = Color.argb(
                    (Color.alpha(lightColour) * 0.5).toInt(),
                    Color.red(lightColour),
                    Color.green(lightColour),
                    Color.blue(lightColour)
                )

            }
        }
    }
}