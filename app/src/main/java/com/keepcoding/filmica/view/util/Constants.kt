package com.keepcoding.filmica.view.util

class Constants {
    companion object {
        val language = "en-US"
        val sort = "popularity.asc"
    }
}