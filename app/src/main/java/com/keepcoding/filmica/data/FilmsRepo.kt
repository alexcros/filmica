package com.keepcoding.filmica.data

import android.arch.persistence.room.Room
import android.content.Context
import android.util.Log
import com.android.volley.Request
import com.android.volley.VolleyError
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

object FilmsRepo {

    private val films: MutableList<Film> = mutableListOf()
    private val trendingFilms: MutableList<Film> = mutableListOf()

    //private val filmsFiltered: MutableList<Film> = mutableListOf()

    @Volatile
    private var db: AppDatabase? = null

    private fun getDbInstance(context: Context): AppDatabase {
        if (db == null) {

            db = Room.databaseBuilder(
                context,
                AppDatabase::class.java,
                "filmica-db"
            ).build()
        }

        return db as AppDatabase
    }

    fun findFilmById(id: String): Film? {
        return films.find { film -> film.id == id }
    }

    fun discoverFilms(
        context: Context,
        page: String,
        callbackSuccess: (MutableList<Film>) -> Unit,
        callbackError: (VolleyError) -> Unit,
        language: String,
        sort: String
    ) {
        if (films.isEmpty()) {
            requestDiscoverFilms(callbackSuccess, callbackError, context, page, language, sort)
        } else {
            callbackSuccess.invoke(films)
        }
    }

    fun discoverAddMoreFilms(
        context: Context,
        page: String,
        callbackSuccess: (MutableList<Film>) -> Unit,
        callbackError: (VolleyError) -> Unit,
        language: String,
        sort: String
    ) {
        if (!films.isEmpty()) {
            requestDiscoverFilms(callbackSuccess, callbackError, context, page, language, sort)
        }
    }

    fun trendingFilms(
        context: Context,
        page: String,
        language: String,
        sort: String,
        callbackSuccess: ((MutableList<Film>) -> Unit),
        callbackError: ((VolleyError) -> Unit)
    ) {
        if (trendingFilms.isEmpty()) {
            // TODO: IMPROVE THIS
            requestTrendingFilms(callbackSuccess, callbackError, context, language, sort, page)
        } else {
            callbackSuccess.invoke(films)
        }

//        else if (trendingFilms.count() > 0) {
//            callbackSuccess.invoke(films)
//        }

    }

    fun trendingAddMoreFilms(
        context: Context,
        page: String,
        callbackSuccess: (MutableList<Film>) -> Unit,
        callbackError: (VolleyError) -> Unit,
        language: String,
        sort: String
    ) {
        if (!films.isEmpty()) {
            requestTrendingFilms(callbackSuccess, callbackError, context, page, language, sort)
        }
    }

    fun saveFilm(
        context: Context,
        film: Film,
        callbackSuccess: (Film) -> Unit
    ) {
        GlobalScope.launch(Dispatchers.Main) {
            val async = async(Dispatchers.IO) {
                val db = getDbInstance(context)
                db.filmDao().insertFilm(film)
            }

            async.await()
            callbackSuccess.invoke(film)
        }
    }

    // saveAllFilms

    fun watchlist(
        context: Context,
        callbackSuccess: (List<Film>) -> Unit
    ) {
        GlobalScope.launch(Dispatchers.Main) {
            val async = async(Dispatchers.IO) {
                val db = getDbInstance(context)
                db.filmDao().getFilms()
            }

            val films: List<Film> = async.await()
            callbackSuccess.invoke(films)
        }
    }

    fun deleteFilm(
        context: Context,
        film: Film,
        callbackSuccess: (Film) -> Unit
    ) {
        GlobalScope.launch(Dispatchers.Main) {
            val async = async(Dispatchers.IO) {
                val db = getDbInstance(context)
                db.filmDao().deleteFilm(film)
            }
            async.await()
            callbackSuccess.invoke(film)
        }
    }

    private fun requestDiscoverFilms(
        callbackSuccess: (MutableList<Film>) -> Unit,
        callbackError: (VolleyError) -> Unit,
        context: Context,
        page: String,
        language: String,
        sort: String
    ) {
        val url = ApiRoutes.discoverUrl(language, sort, page)
        val request = JsonObjectRequest(Request.Method.GET, url, null,
            { response ->
                val newFilms = Film.parseFilms(response)
                films.addAll(newFilms)
                callbackSuccess.invoke(films)
            },
            { error ->
                callbackError.invoke(error)
            })

        Volley.newRequestQueue(context)
            .add(request)
    }

    private fun requestTrendingFilms(
        callbackSuccess: (MutableList<Film>) -> Unit,
        callbackError: (VolleyError) -> Unit,
        context: Context,
        language: String,
        sort: String,
        page: String
    ) {
        val url = ApiRoutes.getTrendingMovies(language, sort, page)
        val request = JsonObjectRequest(Request.Method.GET, url, null,
            { response ->
                val newFilms = Film.parseFilms(response)
                trendingFilms.addAll(newFilms)
                callbackSuccess.invoke(trendingFilms)
            },
            { error ->
                callbackError.invoke(error)
            })

        Volley.newRequestQueue(context)
            .add(request)
    }

    fun searchMovies(
        query: String,
        context: Context,
        callbackSuccess: (MutableList<Film>) -> Unit,
        callbackError: (VolleyError) -> Unit
    ) { // TODO: improve this
        val filmsFiltered: MutableList<Film> = mutableListOf()
        val url = ApiRoutes.searchMovies(query, page = 1)
        Log.i("ApiRoutes.searchMovies", url.toString())
        val request = JsonObjectRequest(Request.Method.GET, url, null,
            { response ->
                val filsmFound = Film.parseSearchFilms(response) //parseFilms(response)
                filmsFiltered.addAll(filsmFound)
                callbackSuccess.invoke(filmsFiltered)
            },
            { error ->
                callbackError.invoke(error)
            })

        Volley.newRequestQueue(context)
            .add(request)
    }

    fun dummyFilms(): List<Film> {
        return (0..9).map { i ->
            Film(
                title = "Film $i",
                overview = "Overview $i",
                genre = "Genre $i",
                voteRating = i.toDouble(),
                release = "200$i-0$i-0$i"
            )
        }
    }
}