package com.keepcoding.filmica.data

import android.net.Uri
import com.keepcoding.filmica.BuildConfig

object ApiRoutes {

    fun discoverUrl(
        language: String = "en-US",
        sort: String = "popularity.asc",
        page: String
    ): String {
        return getUriBuilder()
            .appendPath("discover")
            .appendPath("movie")
            .appendQueryParameter("language", language)
            .appendQueryParameter("page", page)
            .appendQueryParameter("sort_by", sort)
            .appendQueryParameter("include_adult", "false")
            .appendQueryParameter("include_video", "false")
            .build()
            .toString()
    }

    fun getTrendingMovies(
            language: String = "en-US",
            sort: String = "popularity.desc",
            page: String
       ): String {
        return getUriBuilder()
                .appendPath("trending")
                .appendPath("movie")
                .appendPath("day")
                .appendQueryParameter("page", page)
                .build()
                .toString()
    }

    fun searchMovies(
        query: String,
        page: Int = 1
    ): String {
        return getUriBuilder()
            .appendPath("search")
            .appendPath("movie")
            .appendQueryParameter("query", query)
            .appendQueryParameter("page", page.toString())
            .build()
            .toString()
    }

    private fun getUriBuilder() =
        Uri.Builder()
            .scheme("https")
            .authority("api.themoviedb.org")
            .appendPath("3")
            .appendQueryParameter("api_key", BuildConfig.MovieDBApiKey)
}