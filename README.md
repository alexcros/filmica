Filmica: Kotlin app

This app connects to https://api.themoviedb.org/3/

This app will show a list of movies in 3 tabs (Discover, Watchlist and Trending).

Kotlin 1.3.1
Project dependencies

RecyclerView: View
CardView: View
Volley: Networking
Room: Persistence
Coroutines: async tasks
Design: Picasso * Pallete
